#pragma once

typedef enum {
	Red = 1,
	Yellow,
	Black
} Colour;

typedef struct Card {
	Colour col;
	int num;
	struct Card *next;
} Card;


Card* mkcard(Colour, int);
void push(Card**, Card*);
void pop(Card**);
void arr2stack(Card**, Card**, int);
void listcards(Card*);
void shuffle(Card**);
