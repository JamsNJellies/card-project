#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "dat.h"

bool
checkauth(char* name)
{
    FILE *fp;
    char buf[100]; /* names are 99 chrs max, then newline */

    fp = fopen("auth.txt", "r");
    if(!fp) {
        printf(mcantauth);
        exit(EXIT_FAILURE);
    }

    while(fgets(buf,100,fp)) {
        buf[strcspn(buf,"\n")] = 0; /* trim the newline */
        if(strncmp(name,buf,100) == 0) {
            fclose(fp);
            return true;
        }
    }
    
    fclose(fp);
    return false;
}
