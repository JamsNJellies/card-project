#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/* For sleep on windows :/ */
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include "cards.h"
#include "auth.h"
#include "messages.h"

Card *cards;
char p1name[100];
char p2name[100];
int p1score;
int p2score;

bool
chkwin(Card a, Card b)
{
	if(a.col == b.col) {
		return (a.num > b.num) ? true : false;
	} else {
		/* side effect of c enums */
		if(a.col > b.col)
			return true;
		/* edge case for red + black */
		else if(a.col==Red && b.col==Black)
			return true;
		else
			return false;
   	}
		
}

char* 
getcol(Card *c)
{
	switch(c->col) {
		case Red:
			return "red";
			break;
		case Yellow:
			return "yellow";
			break;
	   	case Black:
		 	return "black";
		   	break;
	}

	return "unknown";
}

void
pdraws() 
{
	printf("%s draws a %s %d\n", p1name, getcol(cards), cards->num);
	printf("%s drawn a %s %d\n", p2name, getcol(cards->next), cards->next->num); 
}

void
pscr()
{
	sleep(0.5);
	printf("%s: %dpts %s: %dpts\n", 
		p1name, p1score, 
		p2name, p2score);
}

void
auth(char* name)
{
	printf("Enter your name: "); fflush(stdout);
	fgets(name, 100, stdin);
	name[strcspn(name, "\n")] = 0;

	if(checkauth(name)) {
		printf("Welcome, %s.\n", name);
		return;
	}

	printf(mnoauth, name);
	exit(EXIT_FAILURE);
}

int
main() 
{
	printf(mwelcome);
	auth(p1name);
	auth(p2name);

	while(true) {
		p1score = 0;
		p2score = 0;
		shuffle(&cards);
		
		/* play continues until there are no cards left on the deck */
		while(cards) {
			pdraws();
			if(chkwin(*cards, *cards->next)) {
				printf(mwin, p1name);
				p1score++;
			} else {
				printf(mwin, p2name);
				p2score++;
			}
			pop(&cards); pop(&cards);
			pscr();
		}

		/* TODO: write scores */
		printf(mgameover);
		if (p1score == p2score)
			printf(mdraw);
		else if (p1score > p2score)
			printf(mwin, p1name);
		else
			printf(mwin, p2name);
		
		pscr();
		printf(magain);
		switch(getchar()) {
			case 'n':
			case 'N':
				printf(mquit);
				exit(EXIT_SUCCESS);
		}
	}
}

