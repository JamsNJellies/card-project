#pragma once

/* auth */
bool checkauth(char*);

/* cards */
typedef enum {
	Red = 1,
	Yellow,
	Black
} Colour;

typedef struct Card {
	Colour col;
	int num;
	struct Card *next;
} Card;


Card* mkcard(Colour, int);
void push(Card**, Card*);
void pop(Card**);
void arr2stack(Card**, Card**, int);
void listcards(Card*);
void shuffle(Card**);

/* messages */
#define mwelcome \
"  ____              _    ____                      _ \n"\
" / ___|__ _ _ __ __| |  / ___| __ _ _ __ ___   ___| |\n"\
"| |   / _` | \'__/ _` | | |  _ / _` | \'_ ` _ \\ / _ \\ |\n"\
"| |__| (_| | | | (_| | | |_| | (_| | | | | | |  __/_|\n"\
" \\____\\__,_|_|  \\__,_|  \\____|\\__,_|_| |_| |_|\\___(_)\n"\
"=====================================================\n"

#define mcantauth \
"Can't open auth.txt to check authentication.\n"\
"Please make sure you are running this in the corect directory.\n"\

#define mwin "%s wins!\n";
#define mdraw "It's a draw!\n";
#define magain "Play again? [Y/N]\n";
#define mquit "Goodbye!\n";
#define mgameover "Game Over!\n";
#define mnoauth "You are not authorised.\n";
