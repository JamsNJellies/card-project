char *mwelcome =
"  ____              _    ____                      _ \n"
" / ___|__ _ _ __ __| |  / ___| __ _ _ __ ___   ___| |\n"
"| |   / _` | \'__/ _` | | |  _ / _` | \'_ ` _ \\ / _ \\ |\n"
"| |__| (_| | | | (_| | | |_| | (_| | | | | | |  __/_|\n"
" \\____\\__,_|_|  \\__,_|  \\____|\\__,_|_| |_| |_|\\___(_)\n"
"=====================================================\n";

char *mcantauth =
"Can't open auth.txt to check authentication.\n"
"Please make sure you are running this in the corect directory.\n";

char *mwin = "%s wins!\n";
char *mdraw = "It's a draw!\n";
char *magain = "Play again? [Y/N]\n";
char *mquit = "Goodbye!\n";
char *mgameover = "Game Over!\n";
char *mnoauth = "You are not authorised.\n";
