#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "dat.h"

void 
push(Card **stack, Card *c)
{
	if(*stack) {
		/* If the list exists we need to connect it */
		c->next = *stack;
		*stack = c;
	} else {
		*stack = c; 
	}
}

void
pop(Card **stack)
{
	if(!(*stack)) return;
	Card *temp = (*stack)->next;
	free(*stack);
	*stack = temp; 
}

void
arr2stack(Card **stack, Card *arr[], int len) {

	while(*stack)
		pop(stack);

	for(int i = 0; i<len; i++)
		push(stack, arr[i]);
}

void
shuffle(Card **stack)
{
	Card *temp[30];
	int index = 0;

	for (int i = 1; i<4; i++) { /* Each colour*/
		for(int j = 1; j<11; j++) { /* Each number */
			Card *card = (Card*)calloc(sizeof(Card), 1);
			card->col = i;
			card->num = j;
			temp[index] = card;
			index++;
		}
	}

	srand(time(NULL)); 
	for (int i = 29; i>0; i--) {
		int j = (rand() % (i+1));
		Card *c = temp[i];
		temp[i] = temp[j];
		temp[j] = c;
	}

	/* and now convert it */
	arr2stack(stack, temp, 30);
}
