# Cards Project
My implementation of Task 3 from the OCR Progrogramming Tasks 2020 - 2021

## Compilation

### Linux:
You will need to have meson and ninja installed.
You will also need a C compiler, install one using your distro's package manager.
On Debian and it's derivatives (Ubuntu) the `build-essential` package will install one.
It is probably best to use your distro's package manager for this.
Run `meson build` then `ninja -C build` in the project directory.
The compiled binary is `cards` under the `build` directory.

### Windows:
You will need to install meson and ninja.
On Windows there is a simple msi installer. 
[Latest version as of 2020-03-20l.](https://github.com/mesonbuild/meson/releases/download/0.54.0/meson-0.54.0-64.msi).
You will also need a C compiler (e.g. MSVC, GCC)
Run `meson build` then `ninja -C build`.
The compiled binary is `cards.exe` under the `build` directory.
